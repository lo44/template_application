Version 1.3
- Ajout de la gestion du nom de l'application dans le nom des tables
- Ajout de la surcharge lutèce pour la gestion des erreur sur AbstractXpage et abstractJspBean (Permet d'afficher les erreur sur les champs)
- Ajout d'une méthode générique find by columns

Version 1.2
- MAJ du abstractDao
- Ajout de TU sur toutes les requêtes SQL générique.

Version 1.1
- Ajout d'un abstractDao avec des méthodes générique pour effectuer les opérations jdbc de base (find all, find by ID, delete by id, insert all, update all)

Version 1.O

Mise en place d'un template pour un projet lutéce.
La structure comprends un core (Service,Dao,Bo,Utilisataires,Constant,etc), un plugin lutèce, un modde share-ressource et un site Lutèce.