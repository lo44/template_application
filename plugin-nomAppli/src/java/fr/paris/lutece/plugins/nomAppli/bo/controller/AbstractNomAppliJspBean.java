/*
 * Copyright (c) 2002-2016, Mairie de Paris
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice
 *     and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright notice
 *     and the following disclaimer in the documentation and/or other materials
 *     provided with the distribution.
 *
 *  3. Neither the name of 'Mairie de Paris' nor 'Lutece' nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * License 1.0
 */
package fr.paris.lutece.plugins.nomAppli.bo.controller;

import fr.paris.lutece.plugins.nomAppli.converter.CustomDateConverter;
import fr.paris.lutece.plugins.nomAppli.payload.Warning;
import fr.paris.lutece.plugins.nomAppli.util.sort.CustomAttributeComparator;
import fr.paris.lutece.plugins.nomAppli.utils.Utils;
import fr.paris.lutece.portal.service.i18n.I18nService;
import fr.paris.lutece.portal.service.util.AppPropertiesService;
import fr.paris.lutece.portal.util.mvc.admin.MVCAdminJspBean;
import fr.paris.lutece.portal.util.mvc.utils.MVCMessage;
import fr.paris.lutece.portal.web.constants.Parameters;
import fr.paris.lutece.util.ErrorMessage;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.converters.IntegerConverter;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Payload;
import javax.validation.groups.Default;
import java.util.*;

/**
 * AbstractNomAppliJspBean JSP Bean abstract class for JSP Bean
 */
public abstract class AbstractNomAppliJspBean extends MVCAdminJspBean
{
    // Rights
    // Marks
    protected static final String                          MARK_MAP_ERRORS       = "map_errors";
    protected static final String                          MARK_WRAPPER_WARNINGS = "wrapper_warnings";
    // JSP
    // Templates
    protected static final String                          TEMPLATE_ERROR        = "/skin/site/page_error500.html";
    // Liste Erreurs et Warning
    private                Map<String, List<ErrorMessage>> mapErrors             = new HashMap<>( );
    private                List<ErrorMessage>              warnings              = new ArrayList<>( );
    // Session
    protected Object mutex;
    // Services
    // Constante

    /**
     * Default constructor
     */
    public AbstractNomAppliJspBean( )
    {
        super( );

        // Enregistrement des Converters
        ConvertUtilsBean convertUtilsBean = BeanUtilsBean.getInstance( ).getConvertUtils( );
        // Re-définition du converter des Integer pour avoir null à la place de 0 si la valeur n'existe pas
        convertUtilsBean.register( new IntegerConverter( null ), Integer.class );
        // Convertisseur pour les dates
        convertUtilsBean.register( new CustomDateConverter( null ), Date.class );

    }

    /**
     * Ajout un système de tri sur la liste passée en paramètre à partir des informations récupérées de la requête (nom de l'attribut sur lequel trier et ordre de tri) Avec en plus la possibilité
     * de préciser un tri par défaut
     *
     * @param request
     *         La requête
     * @param list
     *         La liste à trier
     * @param defaultSortedAttributeName
     *         Le nom de la colonne à trier par défaut
     * @param defaultOrder
     *         L'ordre de tri par défaut
     */
    protected void addSort( HttpServletRequest request, List<?> list, String defaultSortedAttributeName, Boolean defaultOrder )
    {
        String sortedAttributeName = request.getParameter( Parameters.SORTED_ATTRIBUTE_NAME );
        String ascSort = request.getParameter( Parameters.SORTED_ASC );

        // Si pas valeur à trier, alors on trie sur la valeur par défaut
        sortedAttributeName = sortedAttributeName == null ? defaultSortedAttributeName : sortedAttributeName;
        boolean isAscSort = ascSort == null ? defaultOrder : Boolean.parseBoolean( ascSort );

        if ( sortedAttributeName != null )
        {
            Collections.sort( list, new CustomAttributeComparator( sortedAttributeName, isAscSort ) );
        }
    }

    /**
     * Ajout un système de tri sur la liste passée en paramètre à partir des informations récupérées de la requête (nom de l'attribut sur lequel trier et ordre de tri)
     *
     * @param request
     *         La requête
     * @param list
     *         La liste à trier
     */
    protected void addSort( HttpServletRequest request, List<?> list )
    {
        addSort( request, list, null, null );
    }

    /**
     * Return an array of Integer from a properties key (values must be separated by comma)
     *
     * @param key
     *         The property key
     * @return {@link java.util.Arrays} of {@link Integer}
     */
    protected List<Integer> getPropertyArrayInt( final String key )
    {
        String property = AppPropertiesService.getProperty( key );
        List<Integer> values = new ArrayList<>( );
        if ( null != property && property.contains( "," ) )
        {
            String[] listValues = property.split( "," );
            for ( String str : Arrays.asList( listValues ) )
            {
                values.add( Integer.valueOf( str ) );
            }
        }

        return values;
    }

    /**
     * Validate a bean for the default group
     *
     * @param bean
     *         The bean
     * @param <T>
     *         The bean class
     * @return true if validated otherwise false
     */
    protected <T> boolean validateBean( T bean )
    {
        // Appel au validateBean sans validationGroups
        return validateBean( bean, Default.class );
    }

    /**
     * Validate a bean
     *
     * @param bean
     *         The bean
     * @param validationGroups
     *         the group to validate
     * @param <T>
     *         The bean class
     * @return true if validated otherwise false
     */
    protected <T> boolean validateBean( T bean, final Class<?>... validationGroups )
    {

        List<ConstraintViolation<T>> errors = Utils.validate( bean, validationGroups );

        boolean hasError = false;
        for ( ConstraintViolation<T> cv : errors )
        {
            String key = cv.getPropertyPath( ).toString( );
            String message = cv.getMessage( );

            // Liste des payloads
            Set<Class<? extends Payload>> payloads = cv.getConstraintDescriptor( ).getPayload( );
            if ( payloads != null && !payloads.isEmpty( ) )
            {
                // On ne récupère que le premier élément de la liste des payloads
                Class<? extends Payload> payload = payloads.iterator( ).next( );
                // Si c'est le payload Warning, on l'ajoute à la liste des infos
                if ( payload.equals( Warning.class ) )
                {
                    addWarning( message );
                } else if ( payload.equals( Error.class ) )
                {
                    addError( key, message );
                    hasError = true;
                }
            }
            // Si pas de payload alors on considère que par défaut c'est une erreur et on l'ajoute à la map des erreurs
            else
            {
                addError( key, message );
                hasError = true;
            }

        }

        return hasError;
    }

    /**
     * Add an error message. The error message must NOT be an I18n key.
     *
     * @param strField
     *         The field for which there is an error
     * @param strMessage
     *         The message
     */
    protected void addError( String strField, String strMessage )
    {
        MVCMessage mvcMessage = new MVCMessage( strMessage );
        List<ErrorMessage> errorMessages;

        if ( mapErrors.containsKey( strField ) )
        {
            errorMessages = mapErrors.get( strField );
        } else
        {
            errorMessages = new ArrayList<>( );
            mapErrors.put( strField, errorMessages );
        }

        errorMessages.add( mvcMessage );
        super.addError( strMessage ); // Backward compatibility
    }

    /**
     * Add an error message. The error message must be an I18n key.
     *
     * @param strField
     *         The field for which there is an error
     * @param strMessageKey
     *         The message
     * @param locale
     *         The locale to display the message in
     */
    protected void addError( String strField, String strMessageKey, Locale locale )
    {
        addError( strField, I18nService.getLocalizedString( strMessageKey, locale ) );
    }

    /**
     * Add a warning message. The warning message must NOT be an I18n key.
     *
     * @param strMessage
     *         The message
     */
    protected void addWarning( String strMessage )
    {
        warnings.add( new MVCMessage( strMessage ) );
    }

    /**
     * {@inheritDoc }
     */
    @Override
    protected void fillCommons( Map<String, Object> model )
    {
        super.fillCommons( model );
        Map<String, List<ErrorMessage>> mapErrorFieldsCopy = new HashMap<>( mapErrors );
        model.put( MARK_MAP_ERRORS, mapErrorFieldsCopy );
        mapErrors.clear( );
        warnings.clear( );

    }
}
