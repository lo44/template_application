package fr.paris.lutece.plugins.nomAppli.util.sort;

import fr.paris.lutece.portal.service.util.AppLogService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;

public class CustomAttributeComparator implements Comparator<Object>, Serializable
{

    private static final long serialVersionUID = -3526872977069103365L;
    private String  sortedAttribute;
    private boolean isASC;

    /**
     * Constructor
     *
     * @param strSortedAttribute
     *         the name of the attribute on which the sort will be made
     * @param bIsASC
     *         true for the ASC order, false for the DESC order
     */
    public CustomAttributeComparator( String strSortedAttribute, boolean bIsASC )
    {
        sortedAttribute = strSortedAttribute;
        isASC = bIsASC;
    }

    /**
     * Constructor
     *
     * @param strSortedAttribute
     *         the name of the attribute on which the sort will be made
     */
    public CustomAttributeComparator( String strSortedAttribute )
    {
        sortedAttribute = strSortedAttribute;
        isASC = true;
    }

    /**
     * Compare two objects o1 and o2.
     *
     * @param o1
     *         Object
     * @param o2
     *         Object
     * @return < 0 if o1 is before o2 in the alphabetical order 0 if o1 equals o2 > 0 if o1 is after o2
     */
    @Override
    public int compare( Object o1, Object o2 )
    {
        int nStatus = 0;

        Pair<Object, Method> pair1 = getMethod( o1, sortedAttribute );
        Pair<Object, Method> pair2 = getMethod( o2, sortedAttribute );

        if ( pair1.getRight( ) != null && pair2.getRight( ) != null && pair1.getRight( ).getReturnType( ) == pair2.getRight( ).getReturnType( ) )
        {
            try
            {
                Object oRet1 = pair1.getRight( ).invoke( pair1.getLeft( ) );
                Object oRet2 = pair2.getRight( ).invoke( pair2.getLeft( ) );

                String strReturnType = pair1.getRight( ).getReturnType( ).getName( ).toString( );
                Class<?> returnType = pair1.getRight( ).getReturnType( );

                if ( oRet1 == null )
                {
                    if ( oRet2 == null )
                    {
                        nStatus = 0;
                    } else
                    {
                        nStatus = -1;
                    }
                } else
                {
                    if ( oRet2 == null )
                    {
                        nStatus = 1;
                    } else
                    {
                        if ( strReturnType.equals( "java.lang.String" ) )
                        {
                            nStatus = ( ( String ) oRet1 ).toLowerCase( ).compareTo( ( ( String ) oRet2 ).toLowerCase( ) );
                        } else if ( returnType.isPrimitive( ) || isComparable( returnType ) )
                        {
                            nStatus = ( ( Comparable ) oRet1 ).compareTo( ( Comparable ) oRet2 );
                        } else if ( returnType.isEnum( ) )
                        {
                            nStatus = oRet1.toString( ).compareTo( oRet2.toString( ) );
                        }
                    }
                }
            } catch ( IllegalArgumentException e )
            {
                AppLogService.error( e );
            } catch ( IllegalAccessException e )
            {
                AppLogService.error( e );
            } catch ( InvocationTargetException e )
            {
                AppLogService.error( e );
            }
        }

        if ( !isASC )
        {
            nStatus = nStatus * -1;
        }

        return nStatus;
    }

    /**
     * Return the getter method of the object obj for the attribute _strSortedAttribute
     *
     * @param obj
     *         the object
     * @return method Method of the object obj for the attribute _strSortedAttribute
     */
    private Pair<Object, Method> getMethod( Object obj, String sortedAttribute )
    {
        Method method = null;
        String strFirstLetter = sortedAttribute.substring( 0, 1 ).toUpperCase( );

        // L'attribut a trié est de la forme "objet1.objet2.objet3"
        // On split pour récupérer le premier élément
        String[] splitedSortedAttribute = StringUtils.split( sortedAttribute, ".", 2 );
        String firstElement = splitedSortedAttribute[0];

        String strMethodName = "get" + strFirstLetter + firstElement.substring( 1, firstElement.length( ) );

        try
        {
            method = obj.getClass( ).getMethod( strMethodName );

            // S'il y a d'autres éléments, alors on appelle la méthode de façon récursive
            if ( splitedSortedAttribute.length > 1 )
            {
                obj = method.invoke( obj );
                return getMethod( obj, splitedSortedAttribute[1] );
            }

        } catch ( Exception e )
        {
            AppLogService.error( e );
        }

        return Pair.of( obj, method );
    }

    /**
     * Returns <code>true</code> if the class implements {@link Comparable} or extends a super class that implements {@link Comparable}, <code>false</code> otherwise.
     *
     * @param clazz
     *         the class
     * @return <code>true</code> if the class implements {@link Comparable}, <code>false</code> otherwise.
     */
    private boolean isComparable( Class<?> clazz )
    {
        for ( Class<?> interfac : clazz.getInterfaces( ) )
        {
            if ( interfac.equals( Comparable.class ) )
            {
                return true;
            }
        }

        // The class might be extending a super class that implements {@link Comparable}
        Class<?> superClass = clazz.getSuperclass( );

        if ( superClass != null )
        {
            return isComparable( superClass );
        }

        return false;
    }

}
