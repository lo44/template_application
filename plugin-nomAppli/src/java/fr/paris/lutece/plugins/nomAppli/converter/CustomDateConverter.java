package fr.paris.lutece.plugins.nomAppli.converter;

import fr.paris.lutece.plugins.nomAppli.constant.Constants;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateTimeConverter;

import java.util.Date;

/**
 * Converter permettant d'avoir plusieurs patterns de date différents
 */
public class CustomDateConverter extends DateTimeConverter implements Converter
{
    /**
     * Construct a <b>java.util.Date</b> <i>Converter</i> that returns a default value if an error occurs.
     *
     * @param defaultValue
     *         The default value to be returned if the value to be converted is missing or an error occurs converting the value.
     */
    public CustomDateConverter( Object defaultValue )
    {
        super( defaultValue );
    }

    @Override
    public <T> T convert( Class<T> type, Object value )
    {

        if ( value != null )
        {
            String strValue = ( String ) value;
            // Détection du pattern de date yyyy
            boolean isYears = strValue.matches( "\\d{4}" );
            if ( isYears )
            {
                setPattern( Constants.YEAR_FORMAT );
            } else
            {
                setPattern( Constants.DATE_FORMAT );
            }
        }
        return super.convert( type, value );
    }

    @Override
    protected Class<?> getDefaultType( )
    {
        return Date.class;
    }

}
