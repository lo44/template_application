package fr.paris.lutece.plugins.nomAppli.converter;

import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * Implémentation de Converter générique pour les Enums
 */
public class EnumConverter implements Converter
{

    // Impossible d'avoir des vérifications à la compilation, donc suppressWarnings
    @SuppressWarnings( { "unchecked", "rawtypes" } )
    @Override
    public Object convert( Class type, Object value )
    {
        // Si la valeur envoyée est vide on n'essaye pas de convertir en Enum
        if ( StringUtils.isBlank( ( String ) value ) )
        {
            return null;
        }
        return type.cast( Enum.valueOf( type, ( String ) value ) );

    }

}
