package fr.paris.lutece.plugins.nomAppli.bo;


public class Test
{

    private Integer idDemande;

    private String toto;

    private int year;

    public String getToto( )
    {
        return toto;
    }

    public void setToto( String toto )
    {
        this.toto = toto;
    }

    public int getYear( )
    {
        return year;
    }

    public void setYear( int year )
    {
        this.year = year;
    }

    public Integer getIdDemande( )
    {
        return idDemande;
    }

    public void setIdDemande( Integer idDemande )
    {
        this.idDemande = idDemande;
    }
}
