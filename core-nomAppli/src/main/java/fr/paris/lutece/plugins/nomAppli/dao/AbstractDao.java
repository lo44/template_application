package fr.paris.lutece.plugins.nomAppli.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.paris.lutece.plugins.nomAppli.annotation.Transient;
import fr.paris.lutece.plugins.nomAppli.constant.Constants;
import fr.paris.lutece.plugins.nomAppli.exception.TechnicalException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import fr.paris.lutece.plugins.spring.jdbc.core.CustomBeanPropertyRowMapper;

public abstract class AbstractDao<R> implements Serializable
{

    @Autowired
    protected NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    protected JdbcTemplate               jdbcTemplate;
    // The tableName
    protected String                     tableName;
    private String                       objectName;
    // The Claas of the object
    private Class<R>                     classOfObjectClass;

    // CONSTANT SQL
    protected static final String        SELECT      = "SELECT ";
    protected static final String        SET         = " SET ";
    protected static final String        UPDATE      = "UPDATE ";
    protected static final String        INSERT_INTO = "INSERT INTO ";
    protected static final String        VALUES      = " VALUES ";
    protected static final String        DELETE_FROM = "DELETE FROM ";
    protected static final String        FROM        = " FROM ";
    protected static final String        INNER_JOIN  = " INNER JOIN ";
    protected static final String        LEFT_JOIN   = " LEFT JOIN ";
    protected static final String        WHERE       = " WHERE ";
    protected static final String        ORDER_BY    = " ORDER BY ";
    protected static final String        ASC         = " ASC ";
    protected static final String        DESC        = " DESC ";
    protected static final String        GROUP_BY    = " GROUP BY ";
    protected static final String        HAVING      = " HAVING ";
    protected static final String        UNION       = " UNION ";
    protected static final String        NOT_IN      = " NOT IN ";
    protected static final String        IN          = " IN ";
    // attention: pour le mot clé COUNT, ne pas mettre d'espace à droite !
    protected static final String        COUNT       = " COUNT";
    protected static final String        AND         = " AND ";
    protected static final String        ON          = " ON ";
    protected static final String        IS_NULL     = " IS NULL ";
    protected static final String        IS_NOT_NULL = " IS NOT NULL ";
    protected static final String        CURDATE     = " CURDATE() ";
    protected static final String		 BETWEEN	 = " BETWEEN ";
    protected static final String        NOW         = " NOW() ";

    /**
     * Default construtor
     */
    public AbstractDao( )
    {
        this.classOfObjectClass = ( Class<R> ) ( ( ParameterizedType ) getClass( ).getGenericSuperclass( ) ).getActualTypeArguments( )[0];
        this.objectName = getClassCamelCase( ( classOfObjectClass ).getSimpleName( ) );
        this.tableName = Constants.APPLICATION_NAME + "_" + objectName;
    }


    /**
     * Insert a new simple object into the table.
     *
     * @param object
     *            instance of the simple object object to insert
     * @throws TechnicalException
     */
    public void insert( final R object )
    {
        insert( object, tableName );
    }

    /**
     * Insert a new simple object into the table.
     *
     * @param object
     *            instance of the simple object object to insert
     * @param tableName
     *            The name of the Table
     * @throws TechnicalException
     */
    public void insert( final R object, final String tableName )
    {

        Field[] allFields = object.getClass( ).getDeclaredFields( );
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert( jdbcTemplate ).withTableName( tableName );
        MapSqlParameterSource namedParameters = new MapSqlParameterSource( );
        // Put all the private field in the MapSqlParameterSource with their value
        for ( Field field : allFields )
        {
            if ( Modifier.isPrivate( field.getModifiers( ) ) && field.getAnnotation( Transient.class ) == null )
                {
                    Object value = getFieldValue( field, object );
                    namedParameters.addValue( getClassCamelCase( field.getName( ) ), value );
                }
            }

        jdbcInsert.execute( namedParameters );
    }

    /**
     * Insert a new simple object into the table and return the ID of the New object (Use camelCase conversion for tableName and id)
     *
     * @param object
     *            instance of the simple object object to insert
     * @return
     * @throws TechnicalException
     */
    public Integer insertAndReturnKey( final R object )
    {
        return insertAndReturnKey( object, tableName, "id_" + objectName );
    }

    /**
     * Insert a new simple object into the table and return the ID of the New object (Use camelCase conversion for tableName)
     *
     * @param object
     *            instance of the simple object object to insert
     * @param identifier
     *            The identifier of the table with a sequence
     * @return
     * @throws TechnicalException
     */
    public Integer insertAndReturnKey( final R object, final String identifier )
    {
        return insertAndReturnKey( object, tableName, identifier );
    }

    /**
     * Insert a new simple object into the table and return the ID of the New object
     *
     * @param object
     *            instance of the simple object object to insert
     * @param tableName
     *            The name of the Table
     * @param identifier
     *            The identifier of the table with a sequence
     * @throws TechnicalException
     */
    public Integer insertAndReturnKey( final R object, final String tableName, final String identifier )
    {

        Field[] allFields = object.getClass( ).getDeclaredFields( );

        MapSqlParameterSource namedParameters = new MapSqlParameterSource( );
        // Put all the private field in the MapSqlParameterSource with their value
        for ( Field field : allFields )
        {
            if ( Modifier.isPrivate( field.getModifiers( ) ) && field.getAnnotation( Transient.class ) == null )
                {
                    Object value = getFieldValue( field, object );
                    namedParameters.addValue( getClassCamelCase( field.getName( ) ), value );
                }
            }

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert( jdbcTemplate ).withTableName( tableName ).usingGeneratedKeyColumns( identifier );

        // Execute the insert
        return jdbcInsert.executeAndReturnKey( namedParameters ).intValue( );
    }

    /**
     * Update the simple object in the table by it's id (Must be named id_+objectName)
     *
     * @param object
     *            instance of the simple object object to update
     * @throws TechnicalException
     */
    public void update( final R object )
    {
        update( object, tableName );
    }

    /**
     * Update the simple object in the table by it's id (Must be named id_+objectName)
     *
     * @param object
     *            instance of the simple object object to update
     * @param tableName
     *            The name of the table
     * @throws TechnicalException
     */
    public void update( final R object, final String tableName )
    {
        Field[] allFields = object.getClass( ).getDeclaredFields( );
        StringBuilder query = new StringBuilder( );
        if ( tableName != null )
        {
            query.append( UPDATE ).append( tableName );
        } else
        {
            query.append( UPDATE ).append( objectName );
        }
        query.append( SET );

        MapSqlParameterSource namedParameters = new MapSqlParameterSource( );

        String idFieldName = "";
        Object idFieldValue = "";
        int validField = 0;

        for ( int i = 0; i < allFields.length; i++ )
        {
            Field field = allFields[i];
            if ( Modifier.isPrivate( field.getModifiers( ) ) )
            {
                if ( field.getAnnotation( Transient.class ) == null && !field.getName( ).equalsIgnoreCase( "id" + object.getClass( ).getSimpleName( ) ) )
                    {
                        Object value = getFieldValue( field, object );

                        // Si on n'est pas au premier élément, on précède d'une virgule
                        if ( validField != 0 )
                        {
                            query.append( ", " );
                        }

                        validField++;

                        query.append( getClassCamelCase( field.getName( ) ) ).append( "= :" ).append( field.getName( ) );

                        namedParameters.addValue( field.getName( ), value );
                    } else
                    {
                        idFieldName = field.getName( );
                        idFieldValue = getFieldValue( field, object );
                    }
                }
            }

        query.append( WHERE ).append( getClassCamelCase( idFieldName ) ).append( "=" ).append( idFieldValue );
        namedJdbcTemplate.update( query.toString( ), namedParameters );
    }

    /**
     * Delete a simple object from the table by its id (Must be named id_+objectName)
     *
     * @param id
     *            The identifier of the table to delete
     * @throws TechnicalException
     */
    public void delete( final int id )
    {
        delete( id, tableName );
    }

    /**
     * Delete a simple object from the table by its id (Must be named id_+objectName)
     *
     * @param id
     *            The identifier of the table to delete
     * @param tableName
     *            The name of the Table
     */
    public void delete( final int id, final String tableName )
    {
        StringBuilder query = new StringBuilder( );
        query.append( DELETE_FROM ).append( tableName );
        query.append( WHERE ).append( "ID_" ).append( objectName ).append( "=:id" );

        MapSqlParameterSource namedParameters = new MapSqlParameterSource( );
        namedParameters.addValue( "id", id );

        namedJdbcTemplate.update( query.toString( ), namedParameters );
    }

    /**
     * Find the simple object's data from the table by it's id (Must be named id_+objectName)
     *
     * @param id
     *            The identifier
     * @return The instance of the object found in the base or null if not found
     */
    public R findById( final Integer id )
    {
        return findById( id, tableName );
    }

    /**
     * Find the simple object's data from the table by it's id (Must be named id_+objectName)
     *
     * @param id
     *            The identifier
     * @param tableName
     *            The name of the Table
     * @return The instance of the object found in the base or null if not found
     */
    public R findById( final Integer id, final String tableName )
    {

        StringBuilder query = new StringBuilder( );
        query.append( SELECT ).append( " * " );
        query.append( FROM ).append( tableName );
        query.append( WHERE ).append( "ID_" ).append( objectName ).append( "=:id" );

        SqlParameterSource params = new MapSqlParameterSource( "id", id );

        List<R> list = namedJdbcTemplate.query( query.toString( ), params, CustomBeanPropertyRowMapper.newInstance( classOfObjectClass ) );

        return list.isEmpty( ) ? null : list.get( 0 );

    }

    /**
     * Find the simple object's data from the table with a chosen column
     *
     * @param columnName
     *            The name of the chosen column
     * @param value
     *            The value to search in the chosen column
     * @return The instance of the simple object
     */
    public R findByColumn( final String columnName, final Object value )
    {
        return findByColumn( tableName, columnName, value );
    }

    /**
     * Find the simple object's data from the table with a chosen column
     *
     * @param tableName
     *            The name of the table
     * @param columnName
     *            The name of the chosen column
     * @param value
     *            The value to search in the chosen column
     * @return The instance of the simple object or null if not found
     */
    public R findByColumn( final String tableName, final String columnName, final Object value )
    {
        StringBuilder query = new StringBuilder( );
        query.append( SELECT ).append( " * " );
        query.append( FROM ).append( tableName );
        query.append( WHERE ).append( columnName ).append( "=:value" );

        SqlParameterSource params = new MapSqlParameterSource( "value", value.toString( ) );

        List<R> list = namedJdbcTemplate.query( query.toString( ), params, CustomBeanPropertyRowMapper.newInstance( classOfObjectClass ) );

        return list.isEmpty( ) ? null : list.get( 0 );

    }

    /**
     *
     * Delete the simple object's data from the table with a chosen column
     *
     * @param tableName
     *            The name of the table
     * @param columnName
     *            The name of the chosen column
     * @param value
     *            The value to search in the chosen column
     */
    public void deleteByColumn( final String tableName, final String columnName, final Object value )
    {
        StringBuilder query = new StringBuilder( );
        query.append( DELETE_FROM ).append( tableName );
        query.append( WHERE ).append( columnName ).append( "=:value" );

        MapSqlParameterSource namedParameters = new MapSqlParameterSource( );
        namedParameters.addValue( "value", value );

        namedJdbcTemplate.update( query.toString( ), namedParameters );

    }

    /**
     * Find the simple object's data from the table with a chosen column
     *
     * @param properties
     * 	      Each fields and its property
     * @return The instance of the simple object
     */
    public List<R> findByColumns( final Map<String,Object> properties )
    {
        return findByColumns( tableName, properties);
    }

    /**
     * Find the simple object's data from the table with a chosen column
     *
     * @param properties
     * 	      Each fields and its property
     * @return The instance of the simple object
     */
    public List<R> findByColumns( final String tableName, final Map<String,Object> properties )
    {
        StringBuilder query = new StringBuilder( );
        query.append( SELECT ).append( " * " );
        query.append( FROM ).append( tableName );
        query.append( WHERE );

        MapSqlParameterSource params = new MapSqlParameterSource();

        int varCount = 0;
        Set<Entry<String,Object>> entrySet = properties.entrySet();
        for(Entry<String,Object> entry : entrySet){
            String varName = "var" + varCount;
            query.append(entry.getKey()).append("=:").append(varName).append(" ");
            if(entrySet.size() > 1 && varCount < entrySet.size()-1){
                query.append(AND);
            }
            params.addValue(varName,entry.getValue().toString());
            varCount++;
        }

        return namedJdbcTemplate.query( query.toString( ), params, CustomBeanPropertyRowMapper.newInstance( classOfObjectClass ) );
    }

    /**
     * Load the data of all the simple object objects and returns them as a list
     *
     * @return The list which contains the data of all the simple object objects
     */
    public List<R> selectReferentielList( )
    {
        return selectReferentielList( null, null );
    }

    /**
     * Load the data of all the simple object objects and returns them as a list ordered
     *
     * @param order
     *            The column to order by
     * @param sens
     *            The way to be ordered by (asc or desc)
     * @return The list which contains the data of all the simple object objects
     */
    public List<R> selectReferentielList( final String order, final String sens )
    {
        StringBuilder query = new StringBuilder( );
        query.append( SELECT ).append( " * " );
        query.append( FROM ).append( tableName );

        if ( ( null != order ) && ( null != sens ) )
        {
            query.append( ORDER_BY ).append( order ).append( " " ).append( sens );
        }

        return namedJdbcTemplate.getJdbcOperations( ).query( query.toString( ), CustomBeanPropertyRowMapper.newInstance( classOfObjectClass ) );
    }

    /**
     * Convert a name in camelCase to an underscored name in lower case. Any upper case letters are converted to lower case with a preceding
     * underscore.
     *
     * @param name
     *            the string containing original name
     * @return the converted name
     */
    private String getClassCamelCase( String name )
    {
        StringBuilder result = new StringBuilder( );
        if ( ( name != null ) && ( name.length( ) > 0 ) )
        {
            result.append( name.substring( 0, 1 ).toLowerCase( ) );
            for ( int i = 1; i < name.length( ); i++ )
            {
                String s = name.substring( i, i + 1 );
                if ( s.equals( s.toUpperCase( ) ) )
                {
                    result.append( "_" );
                    result.append( s.toLowerCase( ) );
                } else
                {
                    result.append( s );
                }
            }
        }
        return result.toString( );
    }

    /**
     * Call the getter method of the object for the specified field
     *
     * @param field
     *            The field
     * @param object
     *            The object
     * @return The value of the field of the object
     * @throws TechnicalException
     */
    private Object getFieldValue( final Field field, final R object )
    {
        // MZ: Find the correct method
        for ( Method method : object.getClass( ).getMethods( ) )
        {
            if ( method.getName( ).startsWith( "get" ) && ( method.getName( ).length( ) == ( field.getName( ).length( ) + 3 ) )
                    && method.getName( ).toLowerCase( ).endsWith( field.getName( ).toLowerCase( ) ) )
                {
                    // MZ: Method found, run it
                    try
                    {
                        return method.invoke( object );
                    } catch ( IllegalAccessException | InvocationTargetException e )
                    {
                        throw new TechnicalException( e );
                    }
                }
            }

        return null;
    }

}
