package fr.paris.lutece.plugins.nomAppli.exception;

public class TechnicalException extends RuntimeException
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *  default construtor
     * @param cause
     */
    public TechnicalException( Throwable cause )
    {
        super( cause );
    }

    @Override
    public String getMessage( )
    {
        return "Une erreur technique est survenue. Veuillez contacter l'administateur du site.";

    }
}
