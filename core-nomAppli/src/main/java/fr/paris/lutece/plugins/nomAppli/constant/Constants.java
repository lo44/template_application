package fr.paris.lutece.plugins.nomAppli.constant;

/**
 * Constantes de l'application nomAppli
 */
public final class Constants
{

    /**
     * Defaut constructor
     */
    private Constants( )
    {
    }

    // Nom de l'application utilisé pour pre-fixer le nom des tables
    public static final String APPLICATION_NAME = "nomAppli";

    public static final String DATE_FORMAT = "dd/MM/YYYY";
    public static final String YEAR_FORMAT = "YYYY";
}
