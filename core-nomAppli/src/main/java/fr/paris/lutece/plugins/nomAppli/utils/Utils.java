/*
 *
 *  * Copyright (c) 2002-2013, Mairie de Paris
 *  * All rights reserved.
 *  *
 *  * Redistribution and use in source and binary forms, with or without
 *  * modification, are permitted provided that the following conditions
 *  * are met:
 *  *
 *  *  1. Redistributions of source code must retain the above copyright notice
 *  *     and the following disclaimer.
 *  *
 *  *  2. Redistributions in binary form must reproduce the above copyright notice
 *  *     and the following disclaimer in the documentation and/or other materials
 *  *     provided with the distribution.
 *  *
 *  *  3. Neither the name of 'Mairie de Paris' nor 'Lutece' nor the names of its
 *  *     contributors may be used to endorse or promote products derived from
 *  *     this software without specific prior written permission.
 *  *
 *  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 *  * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  * POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  * License 1.0
 *
 */

package fr.paris.lutece.plugins.nomAppli.utils;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Classe utilitaire
 */
public final class Utils
{

    private static Validator validator = Validation.buildDefaultValidatorFactory( ).getValidator( );

    private Utils( )
    {
    }

    /**
     * @param object
     *         L'object à valider
     * @param <T>
     *         Le type de l'objet passé en param
     * @return Une {@link List} de {@link ConstraintViolation}
     */
    public static <T> List<ConstraintViolation<T>> validate( final T object )
    {
        return new ArrayList<ConstraintViolation<T>>( validator.validate( object ) );
    }

    /**
     * @param object
     *         L'object à valider
     * @param property
     *         le field de l'objet à valider
     * @param <T>
     *         Le type de l'objet passé en param
     * @return Une {@link List} de {@link ConstraintViolation}
     */
    public static <T> List<ConstraintViolation<T>> validateProperty( final T object, final String property )
    {
        return new ArrayList<ConstraintViolation<T>>( validator.validateProperty( object, property ) );
    }

    /**
     * @param object
     *         L'object à valider
     * @param validationGroups
     *         Les groups de validation
     * @param <T>
     *         Le type de l'objet passé en param
     * @return Une {@link List} de {@link ConstraintViolation}
     */
    public static <T> List<ConstraintViolation<T>> validate( final T object, final Class<?>... validationGroups )
    {
        return new ArrayList<ConstraintViolation<T>>( validator.validate( object, validationGroups ) );
    }

    /**
     * Make N instances of the given class
     *
     * @param n
     * @param classToInstanciate
     * @return A List populated with N new instances
     */
    public static <T> List<T> makeNInstances( int n, Class<T> classToInstanciate )
    {
        List<T> list = new ArrayList<T>( );
        for ( int i = 0; i < n; i++ )
        {
            try
            {
                T objet = classToInstanciate.newInstance( );
                list.add( objet );
            } catch ( InstantiationException | IllegalAccessException e )
            {
                // Si exception on ne fait rien de particulier
            }
        }
        return list;
    }

    /**
     * Retourne le nombre d'item dans un tableau qui a été envoyé dans un HTTPServletRequest.
     * Exemple : parameter[0].item=aaa parameter[1].item=bbb parameter[2].item=ccc
     *
     * @param parameterName
     *         Le nombre du paramètre sous la forme : parameter[].item
     * @param parameters
     *         La map retournée par httpServletRequest.getParameterMap()
     * @return Le nombre d'item
     */
    public static int getNumberOfItemsInArray( String parameterName, Map<String, String[]> parameters )
    {
        int nbItems = 0;

        // Construction de la regex
        String regex = StringUtils.replace( parameterName, "[].", "\\[\\d+\\]\\." );

        for ( Entry<String, String[]> entry : parameters.entrySet( ) )
        {
            String key = entry.getKey( );
            if ( key.matches( regex ) )
            {
                nbItems++;
            }
        }

        return nbItems;
    }

    /**
     * Xor ternaire, ne retourne true que si un et un seul des 3 booléens est true
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public static boolean ternaryXor( boolean a, boolean b, boolean c )
    {
        return a ^ b ^ c && !( a && b && c );
    }

}
