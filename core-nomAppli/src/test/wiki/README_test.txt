La génération des données de test a été faite en utilisant le site https://mockaroo.com

Pour cela, il suffit d'initialiser les colonnes en copiant le script SQL de création des tables sur : https://mockaroo.com/schemas/new_from_ddl
Puis de modifier le type de données à générer aléatoirement par mockaroo pour refléter au mieux la donnée métier.
Par exemple si l'attribut est un email, il faut utiliser le type "Email Address".
Il est même possible d'utiliser des expressions régulières si aucun type ne correspond au type de donnée.

Une fois le schéma bien configuré, il est possible de le sauvegarder au format JSON pour pouvoir le réimporter plus tard dans mockaroo en cas de modification du schéma.