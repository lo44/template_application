SET @storage_engine='INNODB';

DROP TABLE IF EXISTS business_object;


-- -----------------------------------------------------
-- Table t_test
-- -----------------------------------------------------
CREATE TABLE nom_appli_business_object (
  id_business_object		INT	 			NOT NULL AUTO_INCREMENT	COMMENT 'Id de la table',
  libelle			VARCHAR(50)		NOT NULL 				COMMENT 'Libellé',
  entier			INT				NOT NULL 				COMMENT 'Entier',
  annee				DATE			NOT NULL 				COMMENT 'Date',
  PRIMARY KEY (id_business_object));
