package fr.paris.lutece.plugins.nomAppli.bo;

import java.util.Date;

public class BusinessObject
{

    private Integer idBusinessObject;

    private String  libelle;

    private int     entier;

    private Date    annee;

    /**
     * @return the libelle
     */
    public String getLibelle( )
    {
        return libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle( String libelle )
    {
        this.libelle = libelle;
    }

    /**
     * @return the entier
     */
    public int getEntier( )
    {
        return entier;
    }

    /**
     * @param entier
     *            the entier to set
     */
    public void setEntier( int entier )
    {
        this.entier = entier;
    }

    /**
     * @return the annee
     */
    public Date getAnnee( )
    {
        return annee;
    }

    /**
     * @param annee
     *            the annee to set
     */
    public void setAnnee( Date annee )
    {
        this.annee = annee;
    }

    /**
     * @return the idBusinessObject
     */
    public Integer getIdBusinessObject( )
    {
        return idBusinessObject;
    }

    /**
     * @param idBusinessObject the idBusinessObject to set
     */
    public void setIdBusinessObject( Integer idBusinessObject )
    {
        this.idBusinessObject = idBusinessObject;
    }
}
