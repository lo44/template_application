package fr.paris.lutece.plugins.nomAppli.dao;

import fr.paris.lutece.plugins.nomAppli.bo.BusinessObject;
import fr.paris.lutece.plugins.nomAppli.exception.TechnicalException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.springframework.util.Assert.notEmpty;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "classpath:nomAppli_context-test.xml" } )
@TransactionConfiguration
@Transactional
public class TestAbstractDao
{

    @Autowired
    private DaoImpl testDao;

    @Test
    public void testInsertOK( ) throws TechnicalException
    {
        BusinessObject bo = createBusinessObject( );
        testDao.insert( bo );

        BusinessObject bo2 = testDao.findById( 3 );

        checkBusinessObject( bo2 );

    }

    @Test
    public void testInsertAndReturnKeyOK( ) throws TechnicalException
    {

        BusinessObject bo = createBusinessObject( );
        Integer key = testDao.insertAndReturnKey( bo );

        // Test de la clé
        assertEquals( new Integer( 3 ), key );

        // Test de l'objet inséré en base
        BusinessObject bo2 = testDao.findById( 3 );
        checkBusinessObject( bo2 );

    }

    @Test
    @Ignore
    public void testUpdateOK( ) throws TechnicalException
    {
        BusinessObject bo = testDao.findById( 1 );

        bo.setLibelle( "Nouveau libellé" );
        testDao.update( bo );

        bo = testDao.findById( 1 );
        assertEquals( "Nouveau libellé", bo.getLibelle( ) );

    }

    @Test
    public void testDeleteOK( )
    {

        testDao.delete( 1 );
        BusinessObject bo = testDao.findById( 1 );
        assertNull( bo );
    }

    @Test
    public void testFindByIdOK( )
    {
        BusinessObject bo = testDao.findById( 1 );

        assertNotNull( bo );
        assertEquals( new Integer( 1 ), bo.getIdBusinessObject( ) );
        assertEquals( "test de libelle", bo.getLibelle( ) );
        assertEquals( 9, bo.getEntier( ) );
        // 2016-06-27
        assertEquals( new Date( 1466978400000L ), bo.getAnnee( ) );
    }

    @Test
    public void testFindByIdKO( )
    {
        BusinessObject bo = testDao.findById( 9999 );

        assertNull( bo );
    }

    @Test
    public void testFindByColumnStringOK( )
    {
        BusinessObject bo = testDao.findByColumn( "libelle", "test de libelle" );
        assertNotNull( bo );
        assertEquals( new Integer( 1 ), bo.getIdBusinessObject( ) );
    }

    @Test
    public void testFindByColumnIntOK( )
    {
        BusinessObject bo = testDao.findByColumn( "entier", 9 );
        assertNotNull( bo );
        assertEquals( new Integer( 1 ), bo.getIdBusinessObject( ) );
    }

    @Test
    public void testSelectReferentielListOK( )
    {
        List<BusinessObject> businessObjects = testDao.selectReferentielList( );
        notEmpty( businessObjects );
    }

    /**
     * Méthode helper pour créer un BusinessObject
     *
     * @return
     */
    private BusinessObject createBusinessObject( )
    {
        BusinessObject bo = new BusinessObject( );
        bo.setIdBusinessObject( 3 );
        bo.setEntier( 8 );
        bo.setLibelle( "libellé" );
        bo.setAnnee( new Date( 1466978400000L ) );

        return bo;
    }

    /**
     * Méthode helper pour vérifier que le BusinessObject est bien égal à celui créer par le "createBusinessObject()"
     *
     * @param bo
     */
    private void checkBusinessObject( BusinessObject bo )
    {
        assertNotNull( bo );
        assertEquals( new Integer( 3 ), bo.getIdBusinessObject( ) );
        assertEquals( "libellé", bo.getLibelle( ) );
        assertEquals( 8, bo.getEntier( ) );
        assertEquals( new Date( 1466978400000L ), bo.getAnnee( ) );
    }

    @Test
    public void testFindByColumnsOK( )
    {
        Map<String, Object> properties = new HashMap<>( );
        properties.put( "libelle", "test de libelle" );
        properties.put( "entier", 9 );
        List<BusinessObject> resultList = testDao.findByColumns( properties );
        assertEquals( resultList.size( ) > 0, true );
        BusinessObject bo = resultList.get( 0 );
        assertNotNull( bo );
        assertEquals( new Integer( 1 ), bo.getIdBusinessObject( ) );
    }

    @Test
    public void testFindByColumnsKO( )
    {
        Map<String, Object> properties = new HashMap<>( );
        properties.put( "libelle", "test" );
        properties.put( "entier", 9 );
        List<BusinessObject> resultList = testDao.findByColumns( properties );
        assertEquals( resultList.size( ), 0 );
    }

}
