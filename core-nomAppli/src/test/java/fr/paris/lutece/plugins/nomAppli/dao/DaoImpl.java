package fr.paris.lutece.plugins.nomAppli.dao;

import fr.paris.lutece.plugins.nomAppli.bo.BusinessObject;
import org.springframework.stereotype.Repository;


/**
 * Classe implémentant AbstractDao avec la classe BusinessObject afin de pouvoir la tester
 *
 */
@Repository
public class DaoImpl extends AbstractDao<BusinessObject>
{

}
